# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render_to_response, HttpResponse, render, get_object_or_404
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required, permission_required
from datetime import datetime, timedelta, time
from django.conf import settings as _settings
from Noticia.models import *
from django.db.models import Q
import json
import hashlib


# Create your views here.
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


class Index(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        categorias = Categoria.objects.all()  
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",
                'categorias': categorias
            }
        )

    def post(self, request, *args, **kwargs):
        if request.user.is_staff:
            medio = Medio.objects.filter(
                pagina=request.POST["pagina"],
                categoria_id=request.POST["categoria"],
            )
            if len(medio)>0:
                medio = medio[0]
            else:
                medio = Medio(
                    pagina=request.POST["pagina"],
                    categoria_id=request.POST["categoria"],
                )
                medio.save()
            noticia = Noticia(
                owner=request.user,
                titulo=request.POST["titulo"],
                fecha=request.POST["fecha"],
                img=request.FILES['upload_file'],
                resumen=request.POST["resumen"],
                autor=request.POST["autor"],
                medio=medio,
            )
            noticia.save()
        return HttpResponseRedirect('/')


class NuevaNoticia(TemplateView):
    template_name = "nueva_noticia.html"

    def get(self, request, *args, **kwargs):
        categorias = Categoria.objects.all()  
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",
                'categorias': categorias
            }
        )

    def post(self, request, *args, **kwargs):
        return HttpResponseRedirect('/')


class Administrador(TemplateView):
    template_name = "administrador.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_staff or request.user.is_superuser:
            categorias = Categoria.objects.all()  
            return render(
                request,
                self.template_name,
                {
                    'back_url': "/",
                    'categorias': categorias
                }
            )
        else:
            return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):
        return HttpResponseRedirect('/')


def registro_imagen(request, *args, **kwargs):
    if request.user.is_staff:
        imagen = Imagen(
            categoria_id=request.POST["categoria"],
            titulo=request.POST["titulo"],
            fecha=request.POST["fecha"],
            img=request.FILES['upload_file'],
            description=request.POST["description"],
            tipo="1",
            user=request.user,
        )
        imagen.save()
        content_type = ContentType.objects.get(app_label="Noticia", model="imagen")
        accion = 'El Usuario: %s, Registro La Imagen Con El Id %s, titulo: %s, fecha: %s, description: %s, categoria: %s, tipo: Pagina, pagina: %s' % (request.user.username, imagen.id, imagen.titulo, imagen.fecha, imagen.description, imagen.categoria, imagen.pagina)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=imagen.id,
        )
        bitacora.save()
    return HttpResponseRedirect('/')


class Auth(TemplateView):
    template_name = "auth.html"

    def get(self, request, *args, **kwargs):
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",}
        )

    def post(self, request, *args, **kwargs):
        logout(request)
        username = password = ''
        next = ""
        redirect_to = getattr(_settings, 'LOGIN_REDIRECT_URL', None)
        if request.method == 'GET' and 'next' in request.GET:
            next = request.GET['next']
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if next == "":
                        return HttpResponseRedirect('/')
                    else:
                        return HttpResponseRedirect(next)
            else:
                return HttpResponseRedirect('/auth?error=1')
        return render_to_response(
            'auth.html',
            {
                'username': username,
                'next': next,
            },
        )


class EdictPagina(TemplateView):
    template_name = "EdictPagina.html"

    def get(self, request, pk, *args, **kwargs):
        try:
            pagina = Pagina.objects.get(pk=pk)
            imagen =  Imagen.objects.get(pagina=pagina)
        except Exception as e:
            return HttpResponseRedirect('/Administrador')
        return render(
            request,
            self.template_name,
            {
                'back_url': "/Administrador",
                'pagina': pagina,
                'imagen': imagen,
                'pk': pk,
            }
        )

    def post(self, request, pk, *args, **kwargs):
        pagina = Pagina.objects.filter(pk=pk).update(
            periodico_id=request.POST["pagina_periodico"],
            categoria_id=request.POST["pagina_categoria"],
            numero_de_pagina=request.POST["pagina_nuemro_de_pagina"],
        )
        if len(request.FILES) != 0:
            img = Imagen.objects.filter(
                pagina_id=pk,
            ).update(
                img=request.FILES['pagina_upload_file'],
            )
        pagina = Pagina.objects.get(pk=pk)
        accion = 'El Usuario: %s, Edito El Registro La Pagina Con El Id %s, Numero De Pagina: %s, Categoria: %s, Periodico: %s' % (request.user.username, pagina.id, pagina.numero_de_pagina, pagina.categoria.nombre, pagina.periodico.fecha.strftime('%Y-%m-%d'))
        content_type = ContentType.objects.get(app_label="Noticia", model="pagina")
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=pk,
        )
        bitacora.save()
        return HttpResponseRedirect('/edict_pagina/'+pk)


class EdictNoticia(TemplateView):
    template_name = "EdictNoticia.html"

    def get(self, request, pk, *args, **kwargs):
        try:
            noticia = Noticia.objects.get(pk=pk)
            imagen =  Imagen.objects.get(pagina=noticia.pagina)
        except Exception as e:
            return HttpResponseRedirect('/Administrador')
        return render(
            request,
            self.template_name,
            {
                'back_url': "/Administrador",
                'noticia': noticia,
                'imagen': imagen,
                'cordenadas': cordenadas,
            }
        )

    def post(self, request, pk, *args, **kwargs):
        #area = (float(request.POST["x"]),float(request.POST["y"]),float(request.POST["x"])+float(request.POST["width"]),float(request.POST["y"])+float(request.POST["height"]))
        noticia = Noticia.objects.get(pk=pk)
        noticia.titulo=request.POST["titulo"]
        noticia.autor=request.POST["autor"]
        noticia.resumen=request.POST["resumen"]
        noticia.cuerpo=request.POST["cuerpo"]
        #noticia.pixeles=request.POST["x"]+","+request.POST["y"]+","+str(float(request.POST["x"])+float(request.POST["width"]))+","+str(float(request.POST["y"])+float(request.POST["height"]))
        noticia.save()
        accion = 'El Usuario: %s, Edito La Noticia Con El Id %s, Titulo: %s, Autor: %s, Resumen: %s, Cuerpo: %s' % (request.user.username, noticia.id, noticia.titulo, noticia.autor, noticia.resumen, noticia.cuerpo)
        content_type = ContentType.objects.get(app_label="Noticia", model="noticia")
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=noticia.id,
        )
        bitacora.save()
        noticia.set_img(area)
        return HttpResponseRedirect('/edict_noticia/'+pk)


class Join(TemplateView):
    template_name = "join.html"

    def get(self, request, *args, **kwargs):
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",}
        )

    def post(self, request, *args, **kwargs):
        try:
            user = User(
                first_name=request.POST["firstname"],
                last_name=request.POST["lastname"],
                email=request.POST["email"],
                username=request.POST["username"],
            )
            user.set_password(request.POST["password"])
            user.save()
            content_type = ContentType.objects.filter(Q(model="comentario") | Q(model="valora"))
            permisos = Permission.objects.filter(content_type__in=content_type)
            user.user_permissions.set(permisos)
            login(request, user)
        except Exception as e:
            return HttpResponseRedirect('/join?error=1')
        return HttpResponseRedirect('/')


class DetalleNoticia(TemplateView):
    template_name = "detalle_de_noticia.html"

    def get(self, request, pk, *args, **kwargs):
        noticia = Noticia.objects.get(
            pk=pk,
        )
        self_value = 0
        noticia_de_la_pagina = Noticia.objects.filter(
            pagina=noticia.pagina
        )
        paginas = Pagina.objects.filter(
            periodico=noticia.pagina.periodico
        ).order_by('numero_de_pagina')
        paginas_list = []
        for pagina in paginas:
            paginas_list.append(pagina)
        pagina_index = paginas_list.index(noticia.pagina)
        pagina_anterior = "#"
        pagina_sigiente = "#"
        if len(paginas_list) == 1:
            pass
        elif pagina_index == 0 and len(paginas_list) > 1:
            pagina_sigiente = paginas_list[pagina_index+1].get_notice()
        elif pagina_index > 0 and len(paginas_list) > pagina_index+1:
            pagina_sigiente = paginas_list[pagina_index+1].get_notice()
            pagina_anterior = paginas_list[pagina_index-1].get_notice()
        else:
            pagina_anterior = paginas_list[pagina_index-1].get_notice()
        if request.user.is_authenticated:
            valor = Valora.objects.filter(
                owner=request.user,
                noticia_id=pk
            )
            if valor:
                self_value = valor[0].valor
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",
                'noticia': noticia,
                'pagina_anterior': pagina_anterior,
                'pagina_sigiente': pagina_sigiente,
                'noticia_de_la_pagina': noticia_de_la_pagina,
                'self_value': self_value,
            }
        )


class DetalleImagene(TemplateView):
    template_name = "detalle_de_imagen.html"

    def get(self, request, pk, *args, **kwargs):
        imagen = Imagen.objects.get(
            pk=pk,
        )
        self_value = 0
        if request.user.is_authenticated:
            valor = Valora.objects.filter(
                owner=request.user,
                imagen_id=pk
            )
            if valor:
                self_value = valor[0].valor
        return render(
            request,
            self.template_name,
            {
                'back_url': "/",
                'imagen': imagen,
                'self_value': self_value,
            }
        )


def create_comentario(request, *args, **kwargs):
    comentario = Comentario(
        owner=request.user,
        noticia_id=request.POST.get('noticia', ''),
        imagen_id=request.POST.get('imagen', ''),
        comentario=request.POST['comentario'],
    )
    comentario.save()
    result = json.dumps(comentario.id, ensure_ascii=False)
    return HttpResponse(result, content_type='application/json; charset=utf-8')


def create_noticia(request, *args, **kwargs):
    #area = (float(request.POST["x"]),float(request.POST["y"]),float(request.POST["x"])+float(request.POST["width"]),float(request.POST["y"])+float(request.POST["height"]))
    noticia = Noticia(
        pagina_id=request.POST["pagina_id"],
        titulo=request.POST["titulo"],
        autor=request.POST["autor"],
        resumen=request.POST["resumen"],
        cuerpo=request.POST["cuerpo"],
        #pixeles=request.POST["x"]+","+request.POST["y"]+","+str(float(request.POST["x"])+float(request.POST["width"]))+","+str(float(request.POST["y"])+float(request.POST["height"])),
        pixeles="nada"
    )
    noticia.save()
    accion = 'El Usuario: %s, Registro La Noticia Con El Id %s, Titulo: %s, Autor: %s, Resumen: %s, Cuerpo: %s' % (request.user.username, noticia.id, noticia.titulo, noticia.autor, noticia.resumen, noticia.cuerpo)
    content_type = ContentType.objects.get(app_label="Noticia", model="noticia")
    bitacora = Bitacora(
        owner=request.user,
        accion=accion,
        modelo=content_type,
        object_id=noticia.id,
    )
    bitacora.save()
    #noticia.set_img(area)
    return HttpResponseRedirect('/edict_pagina/'+request.POST["pagina_id"])


def nueva_pagina(request, *args, **kwargs):
    if request.user.is_staff:
        pagina = Pagina(
            periodico_id=request.POST["nueva_pagina_periodico"],
            categoria_id=request.POST["nueva_pagina_categoria"],
            numero_de_pagina=request.POST["nueva_pagina_nuemro_de_pagina"],
        )
        pagina.save()
        img = Imagen(
            titulo="Pagina Numero "+pagina.numero_de_pagina+", Del medio: "+pagina.periodico.medio.nombre+", Fecha: "+pagina.periodico.fecha.strftime('%Y-%m-%d'),
            fecha=pagina.periodico.fecha,
            description="Pagina Numero "+pagina.numero_de_pagina+", Del medio: "+pagina.periodico.medio.nombre+", Fecha: "+pagina.periodico.fecha.strftime('%Y-%m-%d'),
            user=request.user,
            img=request.FILES['nueva_pagina_upload_file'],
            categoria=pagina.categoria,
            tipo="2",
            pagina=pagina,
        )
        img.save()
        content_type = ContentType.objects.get(app_label="Noticia", model="pagina")
        accion = 'El Usuario: %s, Registro La Pagina Con El Id %s, Numero De Pagina: %s, Del Periodico: %s, De La Categoria: %s' % (request.user.username, pagina.id, pagina.numero_de_pagina, pagina.periodico, pagina.categoria)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=pagina.id,
        )
        bitacora.save()
        content_type = ContentType.objects.get(app_label="Noticia", model="imagen")
        accion = 'El Usuario: %s, Registro La Imagen Con El Id %s, titulo: %s, fecha: %s, description: %s, categoria: %s, tipo: Pagina, pagina: %s' % (request.user.username, img.id, img.titulo, img.fecha, img.description, img.categoria, img.pagina)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=img.id,
        )
        bitacora.save()
    return HttpResponseRedirect('/Administrador')


def create_valorar(request, *args, **kwargs):
    valoras = Valora.objects.filter(
        owner=request.user,
        noticia_id=request.POST.get('noticia', None),
        imagen_id=request.POST.get('imagen', None),
    )
    if valoras:
        valora = valoras[0]
        valora.valor = request.POST['valor']
    else:
        valora = Valora(
            owner=request.user,
            noticia_id=request.POST.get('noticia', None),
            imagen_id=request.POST.get('imagen', None),
            valor=request.POST['valor'],
        )
    valora.save()
    recalcular = Valora.objects.filter(
        noticia_id=request.POST.get('noticia', None),
        imagen_id=request.POST.get('imagen', None),
    )
    suma = 0
    if recalcular:
        for calcular in recalcular:
            suma = suma + calcular.valor
    suma = suma/len(recalcular)
    result = json.dumps(suma, ensure_ascii=False)
    return HttpResponse(result, content_type='application/json; charset=utf-8')

