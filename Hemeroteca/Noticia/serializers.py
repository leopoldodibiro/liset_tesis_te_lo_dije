from django.contrib.auth.models import User
from rest_framework import serializers
from Noticia.models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'is_staff', 'first_name', 'last_name')


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = '__all__'


class MedioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medio
        fields = '__all__'


class PeriodicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periodico
        fields = '__all__'


class PeriodicoDepthSerializer(serializers.ModelSerializer):
    
    class Meta:
        depth = 2
        model = Periodico
        fields = '__all__'


class ComentarioSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    create_date = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    class Meta:
        model = Comentario
        fields = '__all__'


class ValoraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Valora
        fields = '__all__'


class ImagenSerializer(serializers.ModelSerializer):
    fecha = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    class Meta:
        depth = 2
        model = Imagen
        fields = '__all__'


class PaginaSerializer(serializers.ModelSerializer):
    imagen = ImagenSerializer(source='pagina', many=True)
    
    class Meta:
        depth = 2
        model = Pagina
        fields = (
            "id",
            "periodico",
            "categoria",
            "numero_de_pagina",
            "imagen",
        )


class PaginaDepthSerializer(serializers.ModelSerializer):
    
    class Meta:
        depth = 2
        model = Pagina
        fields = '__all__'


class NoticiaSerializer(serializers.ModelSerializer):
    create_date = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    update_date = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    pagina = PaginaSerializer()

    class Meta:
        depth = 2
        model = Noticia
        fields = (
            "id",
            "titulo",
            "pagina",
            "create_date",
            "update_date",
            "autor",
            "resumen",
            "cuerpo",
            "pixeles",
            "img",
        )


class BitacoraSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    create_date = serializers.DateTimeField(format="Fecha: %Y-%m-%d, Hora:%H:%M", required=False, read_only=True)
    
    class Meta:
        depth = 2
        model = Bitacora
        fields = '__all__'


