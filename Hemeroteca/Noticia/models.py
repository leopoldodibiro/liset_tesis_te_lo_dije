# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from PIL import Image
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile


class Persona(models.Model):
    user = models.OneToOneField(User)
    status = models.BooleanField(default=False)
    cedula = models.CharField(max_length=500)
    telefono = models.CharField(max_length=500)
    sexo = models.CharField(max_length=500)

    class Meta:
        db_table = 'persona'
        verbose_name_plural = "Personas"
    
    def __unicode__(self):
        return '%s (%s)' % (self.user, self.slug)


class Categoria(models.Model):
    nombre = models.CharField(max_length=500)

    class Meta:
        db_table = 'categoria'
        verbose_name_plural = "Categorias"
    
    def __unicode__(self):
        return '%s' % self.nombre


class Medio(models.Model):
    nombre = models.CharField(max_length=500)

    class Meta:
        db_table = 'medio'
        verbose_name_plural = "medios"
    
    def __unicode__(self):
        return '%s' % self.nombre


class Periodico(models.Model):
    medio = models.ForeignKey(Medio)
    user = models.ForeignKey(User)
    fecha = models.DateField()

    class Meta:
        unique_together = ('medio', 'fecha',)
        db_table = 'periodico'
        verbose_name_plural = "periodicos"
    
    def __unicode__(self):
        return '%s, Medio: %s' % (self.fecha.strftime('%Y-%m-%d'), self.medio.nombre)


class Pagina(models.Model):
    periodico = models.ForeignKey(Periodico)
    categoria = models.ForeignKey(Categoria)
    numero_de_pagina = models.PositiveIntegerField()

    def get_notice(self):
        noticia = Noticia.objects.filter(
            pagina=self
        )
        if noticia:
            return "/DetalleNoticia/" + str(noticia[0].id)
        else:
            return "#"


    class Meta:
        db_table = 'pagina'
        verbose_name_plural = "paginas"
    
    def __unicode__(self):
        return '%s' % self.numero_de_pagina


class Noticia(models.Model):
    pagina = models.ForeignKey(Pagina)
    titulo = models.CharField(max_length=500)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(blank=True, null=True)
    autor = models.CharField(max_length=500)
    resumen = models.TextField(blank=True, null=True)
    cuerpo = models.TextField(blank=True, null=True)
    pixeles = models.TextField(blank=True, null=True)
    img = models.ImageField(upload_to = 'files/recorte_noticia', blank=True, null=True)
    
    def get_valoracion(self):
        suma = 0
        valoraciones = Valora.objects.filter(
            noticia=self
        )
        if valoraciones:
            for valoracion in valoraciones:
                suma = suma + valoracion.valor
            return suma/len(valoraciones)
        else:
            return 0

    def get_img(self):
        imagen = Imagen.objects.filter(
            pagina=self.pagina
        )
        if imagen:
            return imagen[0].img.url
        else:
            return '#'

    def set_img(self, area):
        imagen = Imagen.objects.filter(pagina=self.pagina)
        img = Image.open(imagen[0].img)
        cropped_img = img.crop(area)
        thumb_io = StringIO.StringIO()
        cropped_img.save(thumb_io, format='JPEG')
        thumb_file = InMemoryUploadedFile(thumb_io, None, str(self.id)+'.jpg', 'image/jpeg', thumb_io.len, None)
        self.img = thumb_file
        self.save()
        return True
    
    def image_tag(self):
        return u'<img src="%s" style="width:600px;height:400px;"/>' % (
            self.img.url
        )
    image_tag.short_description = 'Visualizacion De La Imagen'
    image_tag.allow_tags = True
    
    class Meta:
        db_table = 'noticia'
        verbose_name_plural = "Noticias"
    
    def __unicode__(self):
        return '%s (%s)' % (self.titulo, self.autor)


class Imagen(models.Model):
    TYPE_CHOICES = (("1", "Imagen"), ("2", "Pagina"))
    titulo = models.CharField(max_length=500)
    fecha = models.DateTimeField()
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User)
    img = models.ImageField(upload_to = 'files/noticia')
    categoria = models.ForeignKey(Categoria)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(blank=True, null=True)
    tipo = models.CharField(max_length=2, choices=TYPE_CHOICES, blank=True, null=True)
    pagina = models.ForeignKey(Pagina, blank=True, null=True, related_name='pagina')

    def save(self, *args, **kwargs):
        if self.tipo == '1':
            self.noticia = None
        super(Imagen, self).save(*args, **kwargs)
    
    def get_valoracion(self):
        suma = 0
        valoraciones = Valora.objects.filter(
            imagen=self
        )
        if valoraciones:
            for valoracion in valoraciones:
                suma = suma + valoracion.valor
            return suma/len(valoraciones)
        else:
            return 0

    def image_tag(self):
        return u'<img src="%s" style="width:600px;height:400px;"/>' % (
            self.img.url
        )
    image_tag.short_description = 'Visualizacion De La Imagen'
    image_tag.allow_tags = True
    
    class Meta:
        db_table = 'imagen'
        verbose_name_plural = "imagenes"
    
    def __unicode__(self):
        return '%s (%s)' % (self.titulo, self.description)


class Comentario(models.Model):
    owner = models.ForeignKey(User)
    noticia = models.ForeignKey(Noticia, blank=True, null=True)
    imagen = models.ForeignKey(Imagen, blank=True, null=True)
    comentario = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 'comentario'
        verbose_name_plural = "comentarios"
    
    def __unicode__(self):
        return '%s' % self.comentario


class Valora(models.Model):
    owner = models.ForeignKey(User)
    imagen = models.ForeignKey(Imagen, blank=True, null=True)
    noticia = models.ForeignKey(Noticia, blank=True, null=True)
    valor = models.PositiveIntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 'valora'
        verbose_name_plural = "valoras"
    
    def __unicode__(self):
        return '%s' % self.valor


class Bitacora(models.Model):
    owner = models.ForeignKey(User)
    accion = models.TextField(blank=True, null=True)
    modelo = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'bitacora'
        verbose_name_plural = "bitacoras"
    
    def __unicode__(self):
        return 'User: %s, Modelo: %s, Id: %s' % (self.owner, self.modelo, self.object_id)