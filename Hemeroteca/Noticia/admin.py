# -*- coding: utf-8 -*-
from django.contrib import admin
from Noticia.models import *


class NoticiaAdmin(admin.ModelAdmin):
    fields = ( 
        'titulo',
        'update_date',
        'autor',
        'resumen',
        'cuerpo',
        'pixeles',
        'pagina',
        'image_tag',
    )
    readonly_fields = ('image_tag',)
    search_fields = ('titulo', 'categoria__Nombre', )


admin.site.register(Persona)
admin.site.register(Categoria)
admin.site.register(Medio)
admin.site.register(Imagen)
admin.site.register(Periodico)
admin.site.register(Pagina)
admin.site.register(Comentario)
admin.site.register(Valora)
admin.site.register(Bitacora)
admin.site.register(Noticia, NoticiaAdmin)