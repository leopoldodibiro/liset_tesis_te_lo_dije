from rest_framework import viewsets, filters, mixins, generics, status
from rest_framework.views import APIView
from rest_framework.response import Response
from serializers import *
from rest_framework import filters
import django_filters
from django.db.models import Q
from Noticia.models import *
from django.shortcuts import get_object_or_404
from django.contrib.contenttypes.models import ContentType


class GenericApi(object):

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        queryset = self.filter_queryset(queryset)
        filter = {}
        fields = tuple([str(x.name) for x in self.queryset.model._meta.fields])
        query_params = self.request.query_params.dict()
        for key, value in query_params.items():
            key_split = key.split('__')[0]
            if key_split in fields:
                if value.lower() == 'true':
                    value = True
                elif value.lower() == 'false':
                    value = False
                filter.update({key: value})
        try:
            filtered_object = queryset.filter(**filter)
        except:
            filtered_object = []
        serializer = self.get_serializer(filtered_object, many=True)
        return Response(serializer.data)

    def filter_queryset(self, queryset):
        queryset = super(GenericApi, self).filter_queryset(queryset)
        if 'order_by' in self.request.GET:
            queryset = queryset.order_by(self.request.GET['order_by'])
        return queryset


class UserViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class NoticiaViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Noticia.objects.all()
    serializer_class = NoticiaSerializer


class ImagenViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Imagen.objects.all()
    serializer_class = ImagenSerializer


class ComentarioViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer


class ValoraViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Valora.objects.all()
    serializer_class = ValoraSerializer
    

class CategoriaViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
    
    def destroy(self, request, pk=None):
        categoria = Categoria.objects.get(pk=pk)
        content_type = ContentType.objects.get(app_label="Noticia", model="categoria")
        accion = 'El Usuario: %s, Elimino El Reguistro Del Categoria Con El Id %s, Nombre: %s' % (request.user.username, pk, categoria.nombre)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=pk,
        )
        categoria.delete()
        bitacora.save()
        return Response(status=status.HTTP_204_NO_CONTENT) 
    

class MedioViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Medio.objects.all()
    serializer_class = MedioSerializer

    def destroy(self, request, pk=None):
        medio = Medio.objects.get(pk=pk)
        content_type = ContentType.objects.get(app_label="Noticia", model="medio")
        accion = 'El Usuario: %s, Elimino El Reguistro Del Medio Con El Id %s, Nombre: %s' % (request.user.username, pk, medio.nombre)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=pk,
        )
        medio.delete()
        bitacora.save()
        return Response(status=status.HTTP_204_NO_CONTENT) 
    
    

class PeriodicoDepthViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Periodico.objects.all()
    serializer_class = PeriodicoDepthSerializer
    

class PeriodicoViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Periodico.objects.all()
    serializer_class = PeriodicoSerializer

    def create(self, request):
        dato = {}
        dato["fecha"] = request.POST["fecha"]
        dato["medio"] = request.POST["medio"]
        dato["user"] = request.user.id
        serializer = self.get_serializer(data=dato)
        if serializer.is_valid():
            serializer.save()
            periodico = Periodico.objects.get(pk=serializer.data["id"])
            content_type = ContentType.objects.get(app_label="Noticia", model="periodico")
            accion = 'El Usuario: %s, Reguistro El Periodico Con El Id %s, Del la Fecha: %s, Del Medio: %s' % (request.user.username, serializer.data["id"], periodico.fecha.strftime('%Y-%m-%d'), periodico.medio.nombre)
            bitacora = Bitacora(
                owner=request.user,
                accion=accion,
                modelo=content_type,
                object_id=serializer.data["id"],
            )
            bitacora.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        periodico = Periodico.objects.get(pk=pk)
        content_type = ContentType.objects.get(app_label="Noticia", model="periodico")
        accion = 'El Usuario: %s, Elimino El Reguistro Del Periodico Con El Id %s, Del la Fecha: %s, Del Medio: %s' % (request.user.username, pk, periodico.fecha.strftime('%Y-%m-%d'), periodico.medio.nombre)
        bitacora = Bitacora(
            owner=request.user,
            accion=accion,
            modelo=content_type,
            object_id=pk,
        )
        periodico.delete()
        bitacora.save()
        return Response(status=status.HTTP_204_NO_CONTENT) 
        
    

class PaginaViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Pagina.objects.all()
    serializer_class = PaginaSerializer
    

class PaginaDepthViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Pagina.objects.all()
    serializer_class = PaginaDepthSerializer


class BitacoraViewSet(GenericApi, viewsets.ModelViewSet):
    queryset = Bitacora.objects.all()
    serializer_class = BitacoraSerializer


