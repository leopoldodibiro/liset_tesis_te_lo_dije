from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from Noticia.views import *
from Noticia.api import *
from rest_framework import routers


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
router.register(r'noticia',NoticiaViewSet)
router.register(r'imagen',ImagenViewSet)
router.register(r'comentario',ComentarioViewSet)
router.register(r'valora',ValoraViewSet)
router.register(r'categoria',CategoriaViewSet)
router.register(r'medio',MedioViewSet)
router.register(r'periodico',PeriodicoViewSet)
router.register(r'periodico_depth',PeriodicoDepthViewSet)
router.register(r'pagina',PaginaViewSet)
router.register(r'bitacora',BitacoraViewSet)



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^$',
        Index.as_view(),
        name='Inicio'
        ),
    url(r'^nueva_noticia/',
        NuevaNoticia.as_view(),
        name='Nueva Noticia'
        ),
    url(r'^auth/',
        Auth.as_view(),
        name='Autenticacion'
        ),
    url(r'^Administrador/',
        Administrador.as_view(),
        name='Administrador'
        ),
    url(r'^edict_pagina/(?P<pk>[0-9]+)/',
        EdictPagina.as_view(),
        name='edict_pagina'
        ),
    url(r'^edict_noticia/(?P<pk>[0-9]+)/',
        EdictNoticia.as_view(),
        name='edict_noticia'
        ),
    url(r'^join/',
        Join.as_view(),
        name='Join'
        ),
    url(r'^logout/',
        logout_view,
        name='logout'
        ),
    url(r'^reguistro_imagen/',
        registro_imagen,
        name='reguistro_imagen'
        ),
    url(r'^create_comentario/',
        create_comentario,
        name='create_comentario'
        ),
    url(r'^create_noticia/',
        create_noticia,
        name='create_noticia'
        ),
    url(r'^create_valorar/',
        create_valorar,
        name='create_valorar'
        ),
    url(r'^nueva_pagina/',
        nueva_pagina,
        name='nueva_pagina'
        ),
    url(r'^DetalleNoticia/(?P<pk>[0-9]+)/',
        DetalleNoticia.as_view(),
        name='DetalleNoticia'
        ),
    url(r'^DetalleImagene/(?P<pk>[0-9]+)/',
        DetalleImagene.as_view(),
        name='DetalleImagene'
        ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
