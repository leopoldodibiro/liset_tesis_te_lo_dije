var Noticias = angular.module("Noticias", ["angularUtils.directives.dirPagination"]);
var actual = {};

Noticias.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

Noticias.controller('Admin', function($scope, $http) {
  function get_categoria() {
    $http({
      method: 'GET',
      url: '/api/categoria/'
    }).then(function successCallback(response) {
      $scope.categorias = response.data
      setTimeout(function() {
        $('select').material_select();
      }, 3000)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_medio() {
    $http({
      method: 'GET',
      url: '/api/medio/'
    }).then(function successCallback(response) {
      $scope.medios = response.data
      setTimeout(function() {
        $('select').material_select();
      }, 3000)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_periodico() {
    $http({
      method: 'GET',
      url: '/api/periodico_depth/'
    }).then(function successCallback(response) {
      $scope.periodicos = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_pagina() {
    $http({
      method: 'GET',
      url: '/api/pagina/'
    }).then(function successCallback(response) {
      $scope.paginas = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_noticia() {
    $http({
      method: 'GET',
      url: '/api/noticia/'
    }).then(function successCallback(response) {
      $scope.noticias = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_bitacora() {
    $http({
      method: 'GET',
      url: '/api/bitacora/'
    }).then(function successCallback(response) {
      $scope.bitacoras = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  get_categoria();
  get_medio();
  get_periodico();
  get_pagina();
  get_noticia();
  get_bitacora();
  $scope.add_categoria = function () {
    $.ajax({
      url: '/api/categoria/',
      type: 'POST',
      dataType: 'json',
      data: {nombre: $scope.nueva_categoria.nombre},
    })
    .done(function() {
      get_categoria();
      $scope.nueva_categoria.nombre = "";
      $('#modal_reguistro_categoria').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.eliminar_categoria = function (categoria) {
    confirmar = confirm("esta Seguro De Eliminar La Categoria " + categoria.nombre)
    if (confirmar) {
      $.ajax({
        url: '/api/categoria/'+categoria.id,
        type: 'DELETE',
        dataType: 'json',
      })
      .done(function() {
        get_categoria();
      })
      .fail(function() {

      })
      .always(function() {

      });    
    }
  }
  $scope.show_categoria = function (categoria) {
    $scope.editar_categoria = categoria
  }
  $scope.edict_categoria = function () {
    $.ajax({
      url: '/api/categoria/'+$scope.editar_categoria.id+'/',
      type: 'PATCH',
      dataType: 'json',
      data: {nombre: $scope.editar_categoria.nombre},
    })
    .done(function() {
      get_categoria();
      $scope.editar_categoria = {};
      $('#modal_edtiar_categoria').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.add_medio = function () {
    $.ajax({
      url: '/api/medio/',
      type: 'POST',
      dataType: 'json',
      data: {nombre: $scope.nueva_medio.nombre},
    })
    .done(function() {
      get_medio();
      $scope.nueva_medio.nombre = "";
      $('#modal_reguistro_medio').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.eliminar_medio = function (medio) {
    confirmar = confirm("esta Seguro De Eliminar El medio " + medio.nombre)
    if (confirmar) {
      $.ajax({
        url: '/api/medio/'+medio.id,
        type: 'DELETE',
        dataType: 'json',
      })
      .done(function() {
        get_medio();
      })
      .fail(function() {

      })
      .always(function() {

      });    
    }
  }
  $scope.show_medio = function (medio) {
    $scope.editar_medio = medio
  }
  $scope.edict_medio = function () {
    $.ajax({
      url: '/api/medio/'+$scope.editar_medio.id+'/',
      type: 'PATCH',
      dataType: 'json',
      data: {nombre: $scope.editar_medio.nombre},
    })
    .done(function() {
      get_medio();
      $scope.editar_medio = {};
      $('#modal_edtiar_medio').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.add_periodico = function () {
    $.ajax({
      url: '/api/periodico/',
      type: 'POST',
      dataType: 'json',
      data: {
        fecha: $scope.nueva_periodico.fecha,
        medio: $scope.nueva_periodico.medio.id,
      },
    })
    .done(function() {
      get_periodico();
      $scope.nueva_periodico.fecha = "";
      $scope.nueva_periodico.medio = {};
      $('#modal_reguistro_periodico').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.eliminar_periodico = function (periodico) {
    confirmar = confirm("esta Seguro De Eliminar El periodico " + periodico.nombre)
    if (confirmar) {
      $.ajax({
        url: '/api/periodico/'+periodico.id,
        type: 'DELETE',
        dataType: 'json',
      })
      .done(function() {
        get_periodico();
      })
      .fail(function() {

      })
      .always(function() {

      });    
    }
  }
  $scope.show_periodico = function (periodico) {
    $scope.editar_periodico = periodico
    $("#edict_periodico_medio").val(periodico.medio)
  }
  $scope.edict_periodico = function () {
    $.ajax({
      url: '/api/periodico/'+$scope.editar_periodico.id+'/',
      type: 'PATCH',
      dataType: 'json',
      data: {nombre: $scope.editar_periodico.nombre},
    })
    .done(function() {
      get_periodico();
      $scope.editar_periodico = {};
      $('#modal_edtiar_periodico').modal('close');
    })
    .fail(function() {

    })
    .always(function() {

    });    
  }
  $scope.set_periodico = function (periodico) {
    $scope.select_peridico = periodico
  }
  $scope.eliminar_pagina = function (pagina) {
    confirmar = confirm("esta Seguro De Eliminar El pagina " + pagina.numero_de_pagina)
    if (confirmar) {
      $.ajax({
        url: '/api/pagina/'+pagina.id,
        type: 'DELETE',
        dataType: 'json',
      })
      .done(function() {
        get_pagina();
      })
      .fail(function() {

      })
      .always(function() {

      });    
    }
  }
  $scope.eliminar_noticia = function (noticia) {
    confirmar = confirm("esta Seguro De Eliminar la noticia " + noticia.titulo)
    if (confirmar) {
      $.ajax({
        url: '/api/noticia/'+noticia.id,
        type: 'DELETE',
        dataType: 'json',
      })
      .done(function() {
        get_noticia();
      })
      .fail(function() {

      })
      .always(function() {

      });    
    }
  }

});