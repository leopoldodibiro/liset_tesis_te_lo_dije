var Noticias = angular.module("Noticias", ["angularUtils.directives.dirPagination", "angular-input-stars"]);
var actual = {};

Noticias.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

Noticias.controller('Comentario', function($scope, $http) {
  function get_coment() {
    delete data_objet["comentario"];
    delete data_objet["valoracion"];
    $http({
      method: 'GET',
      url: '/api/comentario/',
      params: data_objet
    }).then(function successCallback(response) {
      $scope.comentarios = response.data
      $scope.set_page = parseInt(response.data.length/6)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  get_coment()
  $scope.submit_comentarios = function() {
    if (/\S/.test($scope.comentario)) {
      var params = data_objet
      params.comentario=$scope.comentario
      $.ajax({
        url: '/create_comentario/',
        type: 'POST',
        dataType: 'json',
        data: params,
      })
      .done(function() {
        $scope.comentario = ""
        get_coment()
      })
      .fail(function() {
      })
      .always(function() {
      });
      
    }
    
  };
  $scope.submit_valor = function() {
    var params = data_objet
    params.valor=$scope.self_valoracion
    $.ajax({
      url: '/create_valorar/',
      type: 'POST',
      dataType: 'json',
      data: params,
    })
    .done(function(data) {
      $scope.valoracion = parseInt(data)
    })
    .fail(function() {
    })
    .always(function() {
    });

  };
});