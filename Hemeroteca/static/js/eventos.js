var Noticias = angular.module("Noticias", ["angularUtils.directives.dirPagination"]);
var actual = {};

Noticias.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

function parseDate(input) {
  var parts = input.split('-');
  // Note: months are 0-based
  return new Date(parts[0], parts[1]-1, parts[2]); 
}


Noticias.controller('listado', function($scope, $http) {
  $http({
    method: 'GET',
    url: '/api/noticia/'
  }).then(function successCallback(response) {
    $scope.noticias = response.data
    // this callback will be called asynchronously
    // when the response is available
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
  $http({
    method: 'GET',
    url: '/api/imagen?tipo=1'
  }).then(function successCallback(response) {
    $scope.imagens = response.data
      // this callback will be called asynchronously
    // when the response is available
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
  function get_categoria() {
    $http({
      method: 'GET',
      url: '/api/categoria/'
    }).then(function successCallback(response) {
      $scope.categorias = response.data
      setTimeout(function() {
        $('select').material_select();
      }, 3000)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  get_categoria()
});

Noticias.filter('dateRange', function() {
    return function( items, fromDate, toDate ) {
      if (fromDate && toDate) {
        var filtered = [];
        //here you will have your desired input
        var from_date = Date.parse(fromDate);
        var to_date = Date.parse(toDate);
        angular.forEach(items, function(item) {
            if(Date.parse(item.fecha) >= from_date && Date.parse(item.fecha) <= to_date) {
                filtered.push(item);
            }
        });
        return filtered;
      }else{
        return items;
      }
    };
});
