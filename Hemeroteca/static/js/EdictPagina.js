var Noticias = angular.module("Noticias", ["angularUtils.directives.dirPagination"]);
var actual = {};

Noticias.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

Noticias.controller('EdictPagina', function($scope, $http) {
  function get_categoria() {
    $http({
      method: 'GET',
      url: '/api/categoria/'
    }).then(function successCallback(response) {
      $scope.categorias = response.data
      setTimeout(function() {
        $("#pagina_categoria").val(pagina_categoria_id)
        $('select').material_select();
      }, 3000)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_periodico() {
    $http({
      method: 'GET',
      url: '/api/periodico_depth/'
    }).then(function successCallback(response) {
      $scope.periodicos = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_medio() {
    $http({
      method: 'GET',
      url: '/api/medio/'
    }).then(function successCallback(response) {
      $scope.medios = response.data
      setTimeout(function() {
        $('select').material_select();
      }, 3000)
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_pagina() {
    $http({
      method: 'GET',
      url: '/api/pagina/'+pagina_id
    }).then(function successCallback(response) {
      $scope.pagina = response.data
      $scope.select_peridico = response.data.periodico
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  function get_noticia() {
    $http({
      method: 'GET',
      url: '/api/noticia?pagina__id='+pagina_id
    }).then(function successCallback(response) {
      $scope.noticias = response.data
        // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }
  get_noticia()
  get_periodico();
  get_categoria();
  get_medio();
  get_pagina()
  $scope.set_periodico = function (periodico) {
    $scope.select_peridico = periodico
  }

});